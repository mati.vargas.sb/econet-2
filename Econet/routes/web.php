<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PrestationsController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RendezvousController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});

Route::get('prestations', PrestationsController::class)->name('prestations');

Route::get('contact', ContactController::class)->name('contact');

Route::get('rendez-vous', RendezvousController::class)->name('rendez-vous');

Route::get('post', [PostController::class, 'index'])->name('post.index');