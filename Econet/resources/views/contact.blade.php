@extends('layouts.home')

@section('title', 'Contact')
    
@section('content')

    <x-form-pages>
        <x-form />
    </x-form-pages>
    <hr class="mx-auto">

@endsection