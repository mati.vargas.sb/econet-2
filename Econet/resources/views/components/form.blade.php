<form action="" class="text-center mx-auto pt-10 max-w-xs">
    <input 
        type="text"
        name="nom"
        placeholder="Nom..."
        class="pl-4 placeholder:italic bg-white rounded-md w-full h-12 text-zinc-700 shadow-lg mb-10 border-gray-200">
    <input 
        type="text"
        name="email"
        placeholder="E-mail..."
        class="pl-4 placeholder:italic bg-white rounded-md w-full h-12 text-zinc-700 shadow-lg mb-10 border-gray-200">
    <input 
        type="text"
        name="sujet"
        placeholder="Sujet..."
        class="pl-4 placeholder:italic bg-white rounded-md w-full h-12 text-zinc-700 shadow-lg mb-10 border-gray-200">
    <textarea 
        name="" 
        id=""
        cols="30"
        rows="10"
        class="pl-4 placeholder:italic bg-white rounded-md w-full text-zinc-700 shadow-lg mb-10 border-gray-200">
        </textarea>

    <x-button>
        Submit
    </x-button>
</form>