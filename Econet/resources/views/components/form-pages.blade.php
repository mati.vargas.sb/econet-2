<section class="bg-zinc-900 min-h-screen p-20">
    <div class=" max-w-6xl bg-slate-200 grid grid-cols-2 mx-auto shadow-md shadow-slate-800 rounded-lg">
        <div class="">
            <img src="./img/cuisine9.jpg" alt="form-photo" class="rounded-l-lg">
        </div>
        <div class="col-span-1-col-start-2 p-10">
            {{$slot}}
        </div>
    </div>
</section>