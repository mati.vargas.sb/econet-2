<footer id="footer" class="bg-zinc-900 text-white text-lg">
    <section class="max-w-6xl mx-auto p-12 flex flex-col sm:flex-row sm:justify-between">
        <address>
            {{-- CAMBIAR POR EMOTICONES DE RRSS!!!!! --}}
            <h2>Econet Antilles</h2>
            Immeuble Sud ZAC de Houelbourg, Baie-Mahault<br>
            {{-- Email: <a href="mailto:inquiries@acmerockets.com">Inquiries@acmerockets.com</a><br> --}}
            Phone: <a href="tel:+330659948300">(+33) 6 59 94 83 00</a>
        </address>
        <nav class="hidden md:flex flex-col gap-2" aria-label="footer">
            <a href="#rockets" class="hover:opacity-70">Nos prestations</a>
            <a href="#testimonials" class="hover:opacity-70">Conseils d'experts</a>
            <a href="#contact" class="hover:opacity-70">Contact</a>
        </nav>
        <div class="flex flex-col sm:gap-2">
            <p class="text-right">Copyright &copy <span id="year">2023</span></p>
            <p class="text-right">All rights reserved</p>
        </div>
    </section>
</footer>