<x-app-layout>
    <div class="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
        <div class="grid-cols-3 gap-6 bg-slate-400">
            @livewire('show-posts')
        </div>
    </div>

    <div>
        @foreach ($posts as $post)
            <div>
                <img src="" alt="">
            </div>
        @endforeach
    </div>
</x-app-layout>
