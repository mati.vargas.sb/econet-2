@extends('layouts.backup')

@section('title', 'Prestations')

@section('content')

    <main class="max-w-7xl mx-auto">
        <div class="flex items-center">
            @livewire('show-posts')
        </div>
    </main>

@endsection
