{{-- Este lo podria juntar con el de edit post --}}
<div>
    <x-danger-button wire:click="$set('open', true)">
        crear post
    </x-danger-button>

    <x-dialog-modal wire:model="open">

        <x-slot name="title">
            Crear nuevo post
        </x-slot>

        <x-slot name="content">

            @if ($image)
                <img src="{{ $image->temporaryUrl() }}" class="mb-4">
            @endif

            <div class="mb-4">
                <x-label value="titulo del post" />
                {{-- con wire:model.defer evitamos que se actualice automaticamente 
                 y lo haga solo con una accion como un click por ej --}}
                <x-input type="text" class="w-full" wire:model="title" />

                <x-input-error for="title">
                </x-input-error>

            </div>
            <div class="mb-4">
                <x-label value="Contenido del post" />
                <textarea class="form-control" rows="6" wire:model="content"></textarea>

                <x-input-error for="content">
                </x-input-error>
            </div>

            <div>
                <input type="file" wire:model="image" id="{{$identificator}}">
                <x-input-error for="image">
                </x-input-error>
            </div>
        </x-slot>

        <x-slot name="footer">
            <x-secondary-button wire:click="$set('open', false)">
                Cancelar
            </x-secondary-button>
            <x-danger-button 
            wire:click="save" 
            wire:loading.attr="disabled" 
            wire:target="save, image"
            class="disabled:opacity-25">
                Crear Post
            </x-danger-button>

        </x-slot>
    </x-dialog-modal>
</div>
