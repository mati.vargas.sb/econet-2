<div>
    <x-table>
        <div class="flex items-center">
            <span>
                Mostrar
            </span>
            <select class="mx-2 form-control" wire:model="quantity">
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
            <span>Entradas</span>
        </div>
        {{-- esta div crea un buscador, es un input que busca lo que tiene el modelo livewire de esta view en el parametro search
    livewire model = showposts -> search -> blade = showposts --}}
        <div class="flex items-center">
            <x-input class="flex-1" placeholder="que busca" type="text" wire:model="search" />
            @livewire('create-post')
        </div>

        @if ($posts->count())
            <table class="min-w-full text-left text-sm font-light">
                <thead class="border-b bg-white font-medium dark:border-neutral-500">
                     {{-- tabla recuperada, table head son los nombres de las filas  --}}
                    <tr>
                        <th scope="col" class="cursor-pointer px-6 py-4" wire:click="order('id')">ID</th>
                        <th scope="col" class="cursor-pointer px-6 py-4" wire:click="order('title')">Title</th>
                        <th scope="col" class="cursor-pointer px-6 py-4" wire:click="order('content')">content</th>
                        <th scope="col" class="cursor-pointer px-6 py-4">Edit</th>
                    </tr>
                </thead>
                <tbody>
                    <div>
                        {{--  aqui cada post de la modelo posts lo renderizo en un foreach que me crea las columnas --}}
                        @foreach ($posts as $item)
                            <tr class="border-b bg-neutral-100 dark:border-neutral-500">
                                <td class="px-6 py-4 font-medium">{{ $item->id }}</td>
                                <td class="px-6 py-4">{{ $item->title }}</td>
                                <td class="px-6 py-4">{{ $item->content }}</td>
                                <td class="px-6 py-4">
                                     @livewire('edit-post', ['post' => $post], key($post->id)) 
                                    <a wire:click="edit({{ $item }})" class="text-zinc-500 cursor-pointer">
                                    </a>
                                </td>
                                <td class="px-6 py-4"><img src="{{ $item->image}}" alt=""></td>
                            </tr>
                        @endforeach
                    </div>
                </tbody>
            </table>
        @else
            <div>
                No existen registros
            </div>
        @endif

        @if ($posts->hasPages())
            <div class="px-6 py-3">
                {{ $posts->links() }}
            </div>
        @endif
 
    </x-table>

    <x-dialog-modal wire:model="open_edit">

        <x-slot name="title">
            Editar post {{ $post->title }}
        </x-slot>

        <x-slot name="content">

            <div wire:loading wire:target="image" class="mb-4 bg-red-100 border-red-400">
                <strong class="font-bold">Imagen cargando</strong>
            </div>
            @if ($image)
                <img src="{{ $image->temporaryUrl() }}" class="mb-4">
            @endif

            <div>
                <x-label value="titulo del post" />
                <x-input type="text" class="w-full" wire:model="post.title" />
            </div>

            <div>
                <x-label value="contenido del post" />
                <textarea wire:model="post.content" class="form-control w-full" rows="6"></textarea>
            </div>

            <div>
                <input type="file" wire:model="image" id="{{ $identificator }}">
                <x-input-error for="image">
                </x-input-error>
            </div>
        </x-slot>

        <x-slot name="footer">
            <x-secondary-button wire:click="$set('open_edit', false)">
                Cancelar
            </x-secondary-button>
            <x-danger-button wire:click="update" wire:loading.attr="disabled" class="disabled:opacity-25">
                Actualizar
            </x-danger-button>
        </x-slot>

    </x-dialog-modal>

    <div>
        @foreach ($posts as $post)
            <div>
                {{Storage::url($post->image)}}
            </div>
        @endforeach
    </div>
</div>


