<div>
    <a wire:click="$set('open', true)" class="text-zinc-500 cursor-pointer">
        Edit
    </a>

    <x-dialog-modal wire:model="open">

        <x-slot name="title">
            Editar post {{ $post->title }}
        </x-slot>

        <x-slot name="content">

            <div wire:loading wire:target="image" class="mb-4 bg-red-100 border-red-400">
                <strong class="font-bold">Imagen cargando</strong>
            </div>
            @if ($image)
                <img src="{{ $image->temporaryUrl() }}" class="mb-4">
                
            @else
                <img src="{{Storage::url($post->image)}}" alt="">
            @endif

            <div>
                <x-label value="titulo del post" />
                <x-input type="text" class="w-full" wire:model="post.title" />
            </div>

            <div>
                <x-label value="contenido del post" />
                <textarea wire:model="post.content" class="form-control w-full" rows="6"></textarea>
            </div>

            <div>
                <input type="file" wire:model="image" id="{{ $identificator }}">
                <x-input-error for="image">
                </x-input-error>
            </div>
        </x-slot>

        <x-slot name="footer">
            <x-secondary-button wire:click="$set('open', false)">
                Cancelar
            </x-secondary-button>
            <x-danger-button wire:click="save" wire:loading.attr="disabled" class="disabled:opacity-25">
                Actualizar
            </x-danger-button>
        </x-slot>

    </x-dialog-modal>
</div>
