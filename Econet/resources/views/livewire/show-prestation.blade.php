<div class="grid grid-cols-3 gap-6">
    @foreach ($prestations as $prestation)
        <div class='flex items-center justify-center min-h-screen px-2'>
            <div class='w-full max-w-md  mx-auto bg-white rounded-3xl shadow-xl overflow-hidden'>
                <div class='max-w-md mx-auto'>
                    <div class='h-[236px]'>
                        <img src="{{ Storage::url($prestation->image) }}" alt="">
                    </div>
                    <div class='p-4 sm:p-6'>
                        <p class='font-bold text-gray-700 text-[22px] leading-7 mb-1'>{{ $prestation->title }}</p>
                        <p class='text-[#7C7C80] font-[15px] mt-6'>{{ $prestation->content }}</p>


                        <a target='_blank' href='foodiesapp://food/1001'
                            class='block mt-10 w-full px-4 py-3 font-medium tracking-wide text-center capitalize transition-colors duration-300 transform bg-[#FFC933] rounded-[14px] hover:bg-[#FFC933DD] focus:outline-none focus:ring focus:ring-teal-300 focus:ring-opacity-80'>
                            View on foodies
                        </a>
                        <a target='_blank' href="https://apps.apple.com/us/app/id1493631471"
                            class='block mt-1.5 w-full px-4 py-3 font-medium tracking-wide text-center capitalize transition-colors duration-300 transform rounded-[14px] hover:bg-[#F2ECE7] hover:text-[#000000dd] focus:outline-none focus:ring focus:ring-teal-300 focus:ring-opacity-80'>
                            Download app
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
