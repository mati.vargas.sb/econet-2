<?php

namespace App\Http\Livewire;

use Livewire\Component;

class EditPrestation extends Component
{
    public function render()
    {
        return view('livewire.edit-prestation');
    }
}
