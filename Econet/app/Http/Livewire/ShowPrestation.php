<?php

namespace App\Http\Livewire;

use App\Models\Prestation;
use Livewire\Component;
use Illuminate\Support\Facades\Storage;
use Livewire\WithPagination;
use Livewire\WithFileUploads;

class ShowPrestation extends Component
{
    use WithPagination;
    use WithFileUploads;

    public $prestation;
    public $image;
    public $identificator;

    public function mount()
    {
        $this->prestation = new Prestation;
        $this->identificator = rand();
    }
    public function render()
    {
        $prestations = Prestation::all();
        return view('livewire.show-prestation', compact('prestations'));
    }
}
