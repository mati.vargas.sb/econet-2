<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Livewire\WithPagination;

class ShowPosts extends Component
{
    use WithFileUploads;
    /*     para que se cambie de links sin recargar la pagina*/
    use WithPagination;

    public $search;
    public $post;
    public $image;
    public $identificator;
    public $sort = 'id';
    public $direction = 'desc';
    public $open_edit = false;
    public $quantity = '10';
    
    protected $queryString = [
        'quantity' => ['except' => '10'], 
        'sort' => ['except' => 'id'], 
        'search'  => ['except' => ''], 
        'direction' => ['except' => 'desc']
    ];

    protected $rules = [
        'post.title' => 'required',
        'post.content' => 'required'
    ];

    protected $listeners = ['render' => 'render'];


    /* mount y uppdating son metodos de livewire que se cargan inmediatamente,
    esta en la documentacion de livewire - lifecycles hooks }
    Mount monta los datos antes de renderizar y el updating si le agrego el nombre
    de la variable search, permite que cuando este en una pagina 10 por ej, pueda buscar
    cosas de cualquier pagina*/
    public function mount()
    {
        $this->identificator = rand();
        $this->post = new Post;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        /* buscar por titulo o contenido */
        $posts = Post::where('title', 'like', '%' . $this->search . '%')
            ->orwhere('content', 'like', '%' . $this->search . '%')
            ->orderby($this->sort, $this->direction)
            ->paginate($this->quantity);


        return view('livewire.show-posts', compact('posts'));
    }

    public function order($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }

    public function edit(Post $post)
    {
        $this->post = $post;
        $this->open_edit = true;
    }

    public function update()
    {
        $this->validate();
        if ($this->image) {
            Storage::delete([$this->post->image]);
            $this->post->image = $this->image->store('posts');
        }
        $this->post->save();
        $this->reset('open_edit', 'image');
        $this->identificator = rand();
    }
}
