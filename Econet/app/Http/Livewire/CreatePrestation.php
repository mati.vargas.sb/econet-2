<?php

namespace App\Http\Livewire;

use App\Models\Prestation;
use Livewire\WithFileUploads;
use Livewire\Component;

class CreatePrestation extends Component
{
    use WithFileUploads;
    
    public $open = false;
    public $title;
    public $content;
    public $image;
    public $identificator;
    /* reglas para crear el post */
    protected $rules = [
        'title' => 'required',
        'content' => 'required',
        'image' => 'required|image|max:2040'
    ];

    /*
    Esto es para validacion en tiempo real
    public function updated($propertyName)
    {

        $this->validateOnly($propertyName);
    } */

    public function mount()
    {
        $this->identificator = rand();
    }

    public function save()
    {

        $this->validate();

        $image = $this->image->store('prestations');
        Prestation::create([
            'title' => $this->title,
            'content' => $this->content,
            'image' => $image
        ]);

        $this->reset('open', 'title', 'content');

        $this->identificator = rand();

        $this->emitTo('show-prestations','render');
    }

    public function render()
    {
        return view('livewire.create-prestation');
    }
}